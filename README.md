#### Reports
- Reports are stored as artifacts in json file.
- You can use any browser to read reports.
#### Secrets Detection
- You can use secret detection template or talisman.
- Using talisman will stop the pipeline if there is any findings
#### Speed the pipeline
- You can use cashe to speed up pipeline.
- A cache is one or more files a job downloads and saves. Subsequent jobs that use the same cache don’t have to download the files again, so they execute more quickly.
- I didn't use it here because there is no files or dependencies to cash. 
#### Presentation Link
https://docs.google.com/presentation/d/177DkVRcz8LbgpGWJXYkzw_O8pzPVRMrcRTtSLRkZ0EQ/edit?usp=sharing
